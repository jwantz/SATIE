TITLE:: SATIE basics
summary:: basic SATIE tutorial with examples
categories:: Libraries
related:: Overview/SATIE-Overview, Classes/SatieConfiguration

Section:: First steps

NOTE::SATIE takes advantage of parrallel processing provided by supernova server therefore it is a preferred setup and examples use it by default. ::
Before using SATIE, we need to create a configuration. The configuration will typically specify the speaker layout, number of effect busses to use etc. We can also configure the supernova server options, which are held in serverOptions variable. SATIE will prepare the synthdefs on the server and later we will be able to control their life cycle.

code::
(
// define a server, SATIE needs it
s = Server.supernova.local;
// instantiate a SatieConfiguration. Here we will use a stereo spatializer
~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);
// list possible listeners:
~satieConfiguration.spatPlugins.keys;
// Change SuperCollider's block size (jack server's blocksize must be multiple of this value)
~satieConfiguration.serverOptions.blockSize = 1024;
// instantiate SATIE renderer and pass it the configuration
~satie = Satie.new(~satieConfiguration);
~satie.boot();
s.waitForBoot({
	// list names of generated synthDef
	~satie.generatedSynthDefs;
	// display some information
	s.meter;
	s.makeGui;
	s.plotTree;
})
)
::

The above is somewhat a minimum to get SATIE running. We will use this simple configuration for the examples that follow.
Now, let's make some noise. We can create instances of synths and pass them some initial arguments.

code::
(
/*   create some test sound instances in the default group  */
20.do( { arg item;
	var nodename=("testNode"++"_"++item).asSymbol;
	var synth;
	synth = ~satie.makeSourceInstance(nodename.asSymbol, \misDrone, \default, synthArgs: [\dur: 8500]);
	synth.set(\gainDB, rrand(-70, -18), \sfreq, rrand(100, 1000));
});
)
::

We can set properties to the entire group:

code::
(
~satie.groups[\default].set(\gainDB, -99);
~satie.groups[\default].set(\gainDB, -18, \freq, 220);
)
::

We can also control each instance individually:

code::
// Set random frequency to each instance
(
20.do( {|item|
	var nodename=("testNode"++"_"++item);
	~satie.groupInstances[\default][nodename.asSymbol].set(\freq, rrand(200, 2000));
});
)

// Set different gain level to each instance
(
20.do( {|item|
	var nodename=("testNode"++"_"++item);
	~satie.groupInstances[\default][nodename.asSymbol].set(\gainDB, rrand(-100, -10));
});
)

::

And of course we can change all spatialization parameters in one message:

code::
(
20.do( {|item|
	var nodename=("testNode"++"_"++item);
	~satie.groupInstances[\default][nodename.asSymbol].set(
		\gainDB, rrand(-50, -10),
		\aziDeg, rrand(-90, 90),
		\eleDeg, rrand(-90, 90),
		\delayMs, rrand(1,100),
		\lpHz, rrand(10000, 22050),
		\hpHz, rrand(0.5, 200),
		\spread, rrand(0, 1)
	);
});
)
::

code::

( /*   clean test instances    */
20.do( { |item|
	~satie.cleanInstance(("testNode"++"_"++item).asSymbol);
});
)
::

Section:: Groups

Groups facilitate updating properties of sound objects in bulk. SATIE creates two groups automatically: soft::default:: and soft::defaultFX::. All SATIE synth and effect instances will be placed in the respective group. User can create additional groups.

code::
(
// create a new group
~satie.makeSatieGroup(\synths);
// add some synths to the group and give them random frequencies, loudness and position
20.do( { arg item;
	var nodename=("test_tone"++"_"++item).asSymbol;
	var synth;
	synth = ~satie.makeSourceInstance(nodename.asSymbol, \testtone, \synths, synthArgs: [
		\sfreq, rrand(200, 800),
		\gainDB, rrand(-60, -30),
		\azideg, rrand(-90, 90)]);
});
)
// set volume of all objects
~satie.groups[\synths].set(\gainDB, -60);

// set frequency of all objects
~satie.groups[\synths].set(\sfreq, 360);

// add another instance of a synth to the group
~satie.makeSourceInstance(\solo, \misDrone, \synths, [\gainDB, -20]);

// set parameters of one instance
~satie.groupInstances[\synths][\solo].set(\freq, 800);

// clean instance
~satie.cleanInstance(\solo, \synths);

// delete the group and all its children
~satie.killSatieGroup(\synths);


::

Go back to link::Overview/SATIE-Overview::

Next Section: link::Examples/Effects::