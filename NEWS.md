# SATIE release notes

## SATIE version 1.0.1 (2018-04-19)
### New features
- Support for ambisonics via SC-HOA quark
- Several mappers for each spatialiser
- Far and near field mappers

### Improvements
- Updated documentation
- Various bugfixes
